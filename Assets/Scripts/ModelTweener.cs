﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ModelTweener : MonoBehaviour
{
    public int modelIndex;
    public float rotationSpeed;

    public float clickTimer;
    public bool mouseDown;

    [Header("Model position references")]
    public Transform defaultPosition;
    public Transform meetingTablePosition;
    public Transform midTweenPosition;
    public Transform defaultTweenedPosition;
    public Transform meetingTableTweenedPosition;

    public void SelectModel()
    {
        if (MeetingManager.Instance.meeting)
        {
            transform.DOMoveY(meetingTableTweenedPosition.transform.localPosition.y, 1f);
            transform.DOScale(meetingTableTweenedPosition.transform.localScale, 1f);
        }
        else
        {
            transform.DOMoveY(defaultTweenedPosition.transform.localPosition.y, 1f);
            transform.DOScale(defaultTweenedPosition.transform.localScale, 1f);
        }
    }

    public void DeSelectModel()
    {
        if (MeetingManager.Instance.meeting)
        {
            transform.DOMoveY(meetingTablePosition.transform.localPosition.y, 1f);
            transform.DOScale(meetingTablePosition.transform.localScale, 1f);
            transform.DORotate(meetingTablePosition.transform.localRotation.eulerAngles, 1f);
        }
        else
        {
            transform.DOMoveY(defaultPosition.transform.localPosition.y, 1f);
            transform.DOScale(defaultPosition.transform.localScale, 1f);
            transform.DORotate(defaultPosition.transform.localRotation.eulerAngles, 1f);
        }
    }

    private void Update()
    {
        if (mouseDown)
        {
            clickTimer += Time.deltaTime;
        }
    }

    private void OnMouseDown()
    {
        mouseDown = true;
    }

    private void OnMouseUp()
    {
        mouseDown = false;

        if (clickTimer <= 0.2f)
        {
            clickTimer = 0;

            if(MeetingManager.Instance.meeting)
            {
                ModelsManager.Instance.SelectModelInMeeting(gameObject);
            }
            else
            {
                ModelsManager.Instance.SelectModelForShowcase(gameObject);
            }
        }
        else
        {
            clickTimer = 0;
        }
    }

    public void OnMouseDrag()
    {
        if (ModelsManager.Instance.selectedModel == gameObject)
        {
            float rotationX = Input.GetAxis("Mouse X") * rotationSpeed * Mathf.Deg2Rad;
            float rotationY = Input.GetAxis("Mouse Y") * rotationSpeed * Mathf.Deg2Rad;

            transform.Rotate(Vector3.up, rotationX);
            transform.Rotate(Vector3.forward, -rotationY);

            if (MeetingManager.Instance.meeting)
            {
                PhotonNetworkManager.Instance.SendSelectedModelRotation(transform.rotation.eulerAngles);
            }
        }
    }
}