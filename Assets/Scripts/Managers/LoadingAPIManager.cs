﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingAPIManager : MonoBehaviour
{
    public GameObject submitButton;
    public GameObject notNowButton;
    public GameObject loadingCircle;

    private void Update()
    {
        if (LoginManager.Instance.APILoading)
        {
            if (submitButton.activeInHierarchy && !loadingCircle.activeInHierarchy)
            {
                if (notNowButton != null)
                {
                    if (notNowButton.activeInHierarchy)
                    {
                        notNowButton.SetActive(false);
                    }
                }
                submitButton.SetActive(false);
                loadingCircle.SetActive(true);
            }
        }
        else
        {
            if (!submitButton.activeInHierarchy && loadingCircle.activeInHierarchy)
            {
                if (notNowButton != null)
                {
                    if (!notNowButton.activeInHierarchy)
                    {
                        notNowButton.SetActive(true);
                    }
                }
                submitButton.SetActive(true);
                loadingCircle.SetActive(false);
            }
        }
    }
}