﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class PanelButtonHoverManager : MonoBehaviour
{
    private Vector3 buttonsHighlightedSize = new Vector3(1.05f, 1.05f, 1.05f);
    private Vector3 buttonsDehighlightedSize = Vector3.one;

    private void OnEnable()
    {
        Out();
    }

    public void Over()
    {
        transform.DOScale(buttonsHighlightedSize, 0.25f);
    }
    public void Out()
    {
        transform.DOScale(buttonsDehighlightedSize, 0.25f);
    }
}