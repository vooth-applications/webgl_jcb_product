﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachineHandler : MonoBehaviour
{
    public static StateMachineHandler Instance;

    //Overall APP state machine
    public enum APP_STATE
    {
        USER_AUTHENTICATION,
        IN_APP
    }

    public enum USER_AUTHENTICATION_STATE
    {
        NONE,
        LOGIN_PAGE,
        REGISTRATION_PAGE_1,
        REGISTRATION_PAGE_2,
        REGISTRATION_VERIFICATION_PAGE,
        FORGOT_PASSWORD_PAGE,
        CHANGE_PASSWORD_PAGE
    }

    public enum IN_APP_STATE
    {
        USER_TYPE_SELECTION,
        FREE_ROAM,
        MEETING
    }

    public APP_STATE currentAppState;
    public USER_AUTHENTICATION_STATE currentUserAuthenticationState;
    public IN_APP_STATE currentInAppState;

    private void Awake()
    {
        Instance = this;
    }

    public void AppStateHandler(APP_STATE state)
    {
        currentAppState = state;

        switch (state)
        {
            case APP_STATE.USER_AUTHENTICATION:
                {
                    UserAuthenticationStateMachineHandler(USER_AUTHENTICATION_STATE.LOGIN_PAGE);
                    break;
                }
            case APP_STATE.IN_APP:
                {
                    InAppStateMachineHandler(IN_APP_STATE.USER_TYPE_SELECTION);
                    break;
                }
        }
    }

    public void InAppStateMachineHandler(IN_APP_STATE state)
    {
        currentInAppState = state;

        switch (state)
        {
            case IN_APP_STATE.USER_TYPE_SELECTION:
                {
                    AppManager.Instance.userSelectionPanel.SetActive(true);
                    AppManager.Instance.joystick.SetActive(false);
                    AppManager.Instance.hostUI.SetActive(false);
                    AppManager.Instance.visitorUI.SetActive(false);
                    break;
                }
            case IN_APP_STATE.FREE_ROAM:
                {
                    AppManager.Instance.userSelectionPanel.SetActive(false);

                    AppManager.Instance.joystick.SetActive(true);
                    AudioVideoManager.Instance.StartAudioChat();

                    if (AppManager.Instance.thisAppController == AppManager.Controller.HOST)
                    {
                        AppManager.Instance.hostUI.SetActive(true);
                        AppManager.Instance.visitorUI.SetActive(false);
                    }
                    else
                    {
                        AppManager.Instance.hostUI.SetActive(false);
                        AppManager.Instance.visitorUI.SetActive(true);
                    }
                    break;
                }
            case IN_APP_STATE.MEETING:
                {
                    break;
                }
        }
    }

    public void UserAuthenticationStateMachineHandler(USER_AUTHENTICATION_STATE state)
    {
        currentUserAuthenticationState = state;
        LoginUIManager.Instance.ResetPages();

        switch (state)
        {
            case USER_AUTHENTICATION_STATE.NONE:
                {
                    LoginUIManager.Instance.loginPage.SetActive(false);
                    LoginUIManager.Instance.registrationPage1.SetActive(false);
                    LoginUIManager.Instance.registrationPage2.SetActive(false);
                    LoginUIManager.Instance.registrationVerificationPage.SetActive(false);
                    LoginUIManager.Instance.forgotPasswordPage.SetActive(false);
                    LoginUIManager.Instance.changePasswordPage.SetActive(false);
                    break;
                }
            case USER_AUTHENTICATION_STATE.LOGIN_PAGE:
                {
                    LoginUIManager.Instance.loginPage.SetActive(true);
                    LoginUIManager.Instance.registrationPage1.SetActive(false);
                    LoginUIManager.Instance.registrationPage2.SetActive(false);
                    LoginUIManager.Instance.registrationVerificationPage.SetActive(false);
                    LoginUIManager.Instance.forgotPasswordPage.SetActive(false);
                    LoginUIManager.Instance.changePasswordPage.SetActive(false);
                    break;
                }
            case USER_AUTHENTICATION_STATE.REGISTRATION_PAGE_1:
                {
                    LoginUIManager.Instance.loginPage.SetActive(false);
                    LoginUIManager.Instance.registrationPage1.SetActive(true);
                    LoginUIManager.Instance.registrationPage2.SetActive(false);
                    LoginUIManager.Instance.registrationVerificationPage.SetActive(false);
                    LoginUIManager.Instance.forgotPasswordPage.SetActive(false);
                    LoginUIManager.Instance.changePasswordPage.SetActive(false);
                    break;
                }
            case USER_AUTHENTICATION_STATE.REGISTRATION_PAGE_2:
                {
                    LoginUIManager.Instance.loginPage.SetActive(false);
                    LoginUIManager.Instance.registrationPage1.SetActive(false);
                    LoginUIManager.Instance.registrationPage2.SetActive(true);
                    LoginUIManager.Instance.registrationVerificationPage.SetActive(false);
                    LoginUIManager.Instance.forgotPasswordPage.SetActive(false);
                    LoginUIManager.Instance.changePasswordPage.SetActive(false);
                    break;
                }
            case USER_AUTHENTICATION_STATE.REGISTRATION_VERIFICATION_PAGE:
                {
                    LoginUIManager.Instance.loginPage.SetActive(false);
                    LoginUIManager.Instance.registrationPage1.SetActive(false);
                    LoginUIManager.Instance.registrationPage2.SetActive(false);
                    LoginUIManager.Instance.registrationVerificationPage.SetActive(true);
                    LoginUIManager.Instance.forgotPasswordPage.SetActive(false);
                    LoginUIManager.Instance.changePasswordPage.SetActive(false);
                    break;
                }
            case USER_AUTHENTICATION_STATE.FORGOT_PASSWORD_PAGE:
                {
                    LoginUIManager.Instance.loginPage.SetActive(false);
                    LoginUIManager.Instance.registrationPage1.SetActive(false);
                    LoginUIManager.Instance.registrationPage2.SetActive(false);
                    LoginUIManager.Instance.registrationVerificationPage.SetActive(false);
                    LoginUIManager.Instance.forgotPasswordPage.SetActive(true);
                    LoginUIManager.Instance.changePasswordPage.SetActive(false);
                    break;
                }
            case USER_AUTHENTICATION_STATE.CHANGE_PASSWORD_PAGE:
                {
                    LoginUIManager.Instance.loginPage.SetActive(false);
                    LoginUIManager.Instance.registrationPage1.SetActive(false);
                    LoginUIManager.Instance.registrationPage2.SetActive(false);
                    LoginUIManager.Instance.registrationVerificationPage.SetActive(false);
                    LoginUIManager.Instance.forgotPasswordPage.SetActive(false);
                    LoginUIManager.Instance.changePasswordPage.SetActive(true);
                    break;
                }
        }
    }

    public void BackButtonHandler()
    {
        switch (currentUserAuthenticationState)
        {
            case USER_AUTHENTICATION_STATE.REGISTRATION_PAGE_1:
                {
                    UserAuthenticationStateMachineHandler(USER_AUTHENTICATION_STATE.LOGIN_PAGE);
                    break;
                }
            case USER_AUTHENTICATION_STATE.REGISTRATION_PAGE_2:
                {
                    UserAuthenticationStateMachineHandler(USER_AUTHENTICATION_STATE.REGISTRATION_PAGE_1);
                    break;
                }
            case USER_AUTHENTICATION_STATE.REGISTRATION_VERIFICATION_PAGE:
                {
                    UserAuthenticationStateMachineHandler(USER_AUTHENTICATION_STATE.REGISTRATION_PAGE_2);
                    break;
                }
            case USER_AUTHENTICATION_STATE.FORGOT_PASSWORD_PAGE:
                {
                    UserAuthenticationStateMachineHandler(USER_AUTHENTICATION_STATE.LOGIN_PAGE);
                    break;
                }
            case USER_AUTHENTICATION_STATE.CHANGE_PASSWORD_PAGE:
                {
                    UserAuthenticationStateMachineHandler(USER_AUTHENTICATION_STATE.FORGOT_PASSWORD_PAGE);
                    break;
                }
        }
    }
}