﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
//using WebSocketSharpUnityMod.Net;

public class LoginManager : MonoBehaviour
{
    public static LoginManager Instance;

    public static string baseAPIEndpoint = "https://365opox2o7.execute-api.ap-south-1.amazonaws.com/dev/api/v1";
    //public static string baseAPIEndpoint = "http://13.233.254.105/api/v1";

    // POST request
    public static string loginAPIEndpoint = "/login";

    //POST request
    public static string registerAPIEndpoint = "/register";

    //GET request
    public static string getTokenAPIEndpoint = "/auth?token=";

    //POST request
    public static string forgotPasswordAPIEndpoint = "/forgot_password";

    //POST request
    public static string resetPasswordAPIEndpoint = "/reset";

    [Header("Error message strings")]
    private string loginSuccess = "Login successful";
    private string loginFailure = "Login failed";
    private string registrationSuccess = "Registration successful";
    private string registrationFailure = "Registration failed";
    private string registrationVerificationSuccess = "Verification successful";
    private string registrationVerificationFailure = "Verification failed";
    private string forgotPasswordSuccess = "Forgot password successful";
    private string forgotPasswordFailure = "Forgot password failed";
    private string changePasswordSuccess = "Change password successful";
    private string changePasswordFailure = "Change password failed";

    public bool APILoading;
    public UserRegistration localUserDetails;

    public enum RequestType
    {
        LOGIN,
        REGISTRATION,
        FORGOT_PASSWORD,
        CHANGE_PASSWORD
    }

    private void Awake()
    {
        Instance = this;
    }

    public void LoginUser(string email, string password)
    {
        UserLogin userLogin = new UserLogin
        {
            email = email,
            password = password
        };

        string json = JsonUtility.ToJson(userLogin);
        StartCoroutine(PostRequest(baseAPIEndpoint + loginAPIEndpoint, json, RequestType.LOGIN));
    }

    public void RegisterUserBeforeToken(string name, string email, string companyName, string divisionName, string title, string contactNumber, string password, string avatarAppearance, string confirmPassword)
    {
        UserRegistration user = new UserRegistration
        {
            name = name,
            email = email,
            company = companyName,
            division = divisionName,
            title = title,
            contactNo = contactNumber,
            password = password,
            avatarAppearance = avatarAppearance,
            confirmPassword = confirmPassword
        };

        string json = JsonUtility.ToJson(user);
        StartCoroutine(PostRequest(baseAPIEndpoint + registerAPIEndpoint, json, RequestType.REGISTRATION));
    }

    public void RegisterUserAfterToken(string token)
    {
        StartCoroutine(GetRequest(baseAPIEndpoint + getTokenAPIEndpoint + token));
    }

    public void ForgotPasswordBeforeCode(string email)
    {
        UserForgotPassword userForgotPassword = new UserForgotPassword
        {
            email = email
        };

        string json = JsonUtility.ToJson(userForgotPassword);
        StartCoroutine(PostRequest(baseAPIEndpoint + forgotPasswordAPIEndpoint, json, RequestType.FORGOT_PASSWORD));
    }

    public void ForgotPasswordAfterCode(string email, string token, string password)
    {
        UserResetPassword userResetPassword = new UserResetPassword
        {
            email = email,
            token = token,
            password = password
        };

        string json = JsonUtility.ToJson(userResetPassword);
        StartCoroutine(PostRequest(baseAPIEndpoint + resetPasswordAPIEndpoint, json, RequestType.CHANGE_PASSWORD));
    }

    public IEnumerator PostRequest(string url, string json, RequestType requestType)
    {
        APILoading = true;

        var uwr = new UnityWebRequest(url, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
        uwr.uploadHandler = new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        //Send the request then wait here until it returns
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            APILoading = false;
            Debug.Log("Error While Sending: " + uwr.error);
        }
        else
        {
            APILoading = false;
            UserRequestResponse response = JsonUtility.FromJson<UserRequestResponse>(uwr.downloadHandler.text);

            Debug.Log("Post Response.message : " + response.message);
            Debug.Log("Post Response.status : " + response.status);
            Debug.Log("Post Response.data : " + response.data.name);

            switch (requestType)
            {
                case RequestType.LOGIN:
                    {
                        if (response.status == "1")
                        {
                            // Login success
                            LoginUIManager.Instance.DisplayErrorText(loginSuccess);
                            StateMachineHandler.Instance.UserAuthenticationStateMachineHandler(StateMachineHandler.USER_AUTHENTICATION_STATE.NONE);
                            StateMachineHandler.Instance.AppStateHandler(StateMachineHandler.APP_STATE.IN_APP);

                            Debug.Log("response.data.name : " + response.data.name);
                            Debug.Log("response.data.email : " + response.data.email);
                            Debug.Log("response.data.companyName : " + response.data.company);
                            Debug.Log("response.data.divisionName : " + response.data.division);
                            Debug.Log("response.data.title : " + response.data.title);
                            Debug.Log("response.data.contactNumber : " + response.data.contactNo);
                            Debug.Log("response.data.avatarAppearance : " + response.data.avatarAppearance);

                            localUserDetails = response.data;
                        }
                        else
                        {
                            // Login failed
                            LoginUIManager.Instance.DisplayErrorText(response.message);
                        }
                        break;
                    }
                case RequestType.REGISTRATION:
                    {
                        if (response.status == "1")
                        {
                            // Registration success
                            LoginUIManager.Instance.DisplayErrorText(registrationSuccess);
                            StateMachineHandler.Instance.UserAuthenticationStateMachineHandler(StateMachineHandler.USER_AUTHENTICATION_STATE.REGISTRATION_VERIFICATION_PAGE);
                        }
                        else
                        {
                            // Registration failed
                            LoginUIManager.Instance.DisplayErrorText(response.message);
                        }
                        break;
                    }
                case RequestType.FORGOT_PASSWORD:
                    {
                        if (response.status == "1")
                        {
                            // Forgot password success
                            LoginUIManager.Instance.DisplayErrorText(forgotPasswordSuccess);
                            StateMachineHandler.Instance.UserAuthenticationStateMachineHandler(StateMachineHandler.USER_AUTHENTICATION_STATE.CHANGE_PASSWORD_PAGE);
                        }
                        else
                        {
                            // Forgot password failed
                            LoginUIManager.Instance.DisplayErrorText(response.message);
                        }
                        break;
                    }
                case RequestType.CHANGE_PASSWORD:
                    {
                        if (response.status == "1")
                        {
                            // Change password success
                            LoginUIManager.Instance.DisplayErrorText(changePasswordSuccess);
                            StateMachineHandler.Instance.UserAuthenticationStateMachineHandler(StateMachineHandler.USER_AUTHENTICATION_STATE.LOGIN_PAGE);
                        }
                        else
                        {
                            // Change password failed
                            LoginUIManager.Instance.DisplayErrorText(response.message);
                        }
                        break;
                    }
            }
        }
    }

    public IEnumerator GetRequest(string url)
    {
        APILoading = true;

        UnityWebRequest uwr = UnityWebRequest.Get(url);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            APILoading = false;
            Debug.Log("Error While Sending: " + uwr.error);
        }
        else
        {
            APILoading = false;
            UserRequestResponse response = JsonUtility.FromJson<UserRequestResponse>(uwr.downloadHandler.text);

            Debug.Log("Get Response.message : " + response.message);
            Debug.Log("Get Response.status : " + response.status);

            if (response.status == "1")
            {
                // Registration verification success
                LoginUIManager.Instance.DisplayErrorText(registrationVerificationSuccess);
                StateMachineHandler.Instance.UserAuthenticationStateMachineHandler(StateMachineHandler.USER_AUTHENTICATION_STATE.LOGIN_PAGE);
            }
            else
            {
                // Registration verification failed
                LoginUIManager.Instance.DisplayErrorText(response.message);
            }
        }
    }

    public void ParseUserData(UserRegistration userRegistrationData)
    {

    }
}

[System.Serializable]
public class UserLogin
{
    public string email;
    public string password;
}

[System.Serializable]
public class UserRegistration
{
    public string name;
    public string email;
    public string company;
    public string division;
    public string title;
    public string contactNo;
    public string password;
    public string confirmPassword;
    public string avatarAppearance;
}

[System.Serializable]
public class UserForgotPassword
{
    public string email;
}

[System.Serializable]
public class UserAuthToken
{
    public string authToken;
}

[System.Serializable]
public class UserResetPassword
{
    public string email;
    public string token;
    public string password;
}

[System.Serializable]
public class UserRequestResponse
{
    public UserRegistration data;
    public string message;
    public string status;
}