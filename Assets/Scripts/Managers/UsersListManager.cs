﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class UsersListManager : MonoBehaviour
{
    public static UsersListManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public List<Player> listOfPlayers = new List<Player>();

    public void AddNewPlayer()
    {
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (listOfPlayers.Count > 0)
            {
                if (!listOfPlayers.Contains(player))
                    listOfPlayers.Add(player);
            }
            else
            {
                listOfPlayers.Add(player);
            }
        }

        Debug.Log("List of Player Count by playersList : " + PhotonNetwork.PlayerList.Length);
        Debug.Log("List of Player Count by listOfPlayers : " + listOfPlayers.Count);
    }

    public void RemovePlayer(Player newPlayer)
    {
        if (listOfPlayers.Contains(newPlayer))
            listOfPlayers.Remove(newPlayer);

        Debug.Log("List of Player Count by playersList : " + PhotonNetwork.PlayerList.Length);
        Debug.Log("List of Player Count by listOfPlayers : " + listOfPlayers.Count);
    }
}