﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelsManager : MonoBehaviour
{
    public static ModelsManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public List<GameObject> listOfShowcaseModels;
    public GameObject selectedModel;

    public void SelectModelForShowcase(GameObject currentlySelectedModel)
    {
        if (selectedModel != null)
        {
            if (currentlySelectedModel == selectedModel)
            {
                foreach (GameObject model in listOfShowcaseModels)
                {
                    model.GetComponent<ModelTweener>().DeSelectModel();
                }
                selectedModel = null;
            }
            else
            {
                foreach (GameObject model in listOfShowcaseModels)
                {
                    if (model == currentlySelectedModel)
                    {
                        selectedModel = currentlySelectedModel;
                        model.GetComponent<ModelTweener>().SelectModel();
                    }
                    else
                    {
                        model.GetComponent<ModelTweener>().DeSelectModel();
                    }
                }
            }
        }
        else
        {
            foreach (GameObject model in listOfShowcaseModels)
            {
                if (model == currentlySelectedModel)
                {
                    selectedModel = currentlySelectedModel;
                    model.GetComponent<ModelTweener>().SelectModel();
                }
                else
                {
                    model.GetComponent<ModelTweener>().DeSelectModel();
                }
            }
        }
    }

    public void SelectModelInMeeting(GameObject model)
    {
        if (selectedModel != null)
        {
            if (AppManager.Instance.playerIndex != PhotonNetwork.LocalPlayer.ActorNumber)
            {
                Debug.Log("Cannot control because other user is using it.");

                //if (UIManager.Instance.errorShown)
                //{
                //    StartCoroutine(UIManager.Instance.DisplayerErrorTextAfterDelay("Model selected by other user"));
                //}
                //else
                //{
                //    UIManager.Instance.DisplayErrorText("Model selected by other user");
                //}
            }
            else
            {
                if (selectedModel == model)
                {
                    selectedModel = null;
                }
                else
                {
                    selectedModel = model;
                    AppManager.Instance.playerIndex = PhotonNetwork.LocalPlayer.ActorNumber;
                }
            }
        }
        else
        {
            selectedModel = model;
            AppManager.Instance.playerIndex = PhotonNetwork.LocalPlayer.ActorNumber;
        }

        PhotonNetworkManager.Instance.SendSelectedModelRPC();

        SelectModelLocally();
    }

    public void SelectModelLocally()
    {
        if (selectedModel != null)
        {
            foreach (GameObject model in listOfShowcaseModels)
            {
                if (model == selectedModel)
                {
                    model.GetComponent<ModelTweener>().SelectModel();
                }
                else
                {
                    model.GetComponent<ModelTweener>().DeSelectModel();
                }
            }
        }
        else
        {
            foreach (GameObject model in listOfShowcaseModels)
            {
                model.GetComponent<ModelTweener>().DeSelectModel();
            }
        }
    }
}
