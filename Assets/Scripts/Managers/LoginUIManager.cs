﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class LoginUIManager : MonoBehaviour
{
    public static LoginUIManager Instance;

    [Header("Login UI pages References")]
    public GameObject loginPage;
    public GameObject registrationPage1;
    public GameObject registrationPage2;
    public GameObject registrationVerificationPage;
    public GameObject forgotPasswordPage;
    public GameObject changePasswordPage;

    [Header("Login page UI references")]
    public TMP_InputField loginEmailInputField;
    public TMP_InputField loginPasswordInputField;

    [Header("Registration page 1 UI references")]
    public TMP_InputField registrationNameInputField;
    public TMP_InputField registrationEmailInputField;
    public TMP_InputField registrationPasswordInputField;
    public TMP_InputField registrationConfirmPasswordInputField;
    private string registrationUserName;
    private string registrationUserEmail;
    private string registrationUserPassword;
    private string registrationUserAvatarAppearance = "";
    public GameObject genderSelectionMaleSelectedImage;
    public GameObject genderSelectionFemaleSelectedImage;

    [Header("Registration page 2 UI references")]
    public TMP_InputField registrationCompanyNameInputField;
    public TMP_InputField registrationDivisionNameInputField;
    public TMP_InputField registrationTitleInputField;
    public TMP_InputField registrationContactNumberInputField;

    [Header("Registration verification page UI references")]
    public TMP_InputField verificationCodeInputField;

    [Header("Forgot password page UI references")]
    public TMP_InputField forgotPasswordEmailInputField;

    [Header("Change password page UI references")]
    public string changePasswordEmail;
    public TMP_InputField changePasswordCodeInputField;
    public TMP_InputField changePasswordNewPasswordInputField;
    public TMP_InputField changePasswordConfirmPasswordInputField;

    [Header("Error text component references")]
    public GameObject errorTextParentComponent;
    public Image errorTextBGImage;
    public TextMeshProUGUI errorText;

    [Header("Error message strings")]
    private string invalidEmail = "Invalid email.";
    private string emptyEmailField = "Email field is mandatory.";
    private string emptyNameField = "Name field is mandatory.";
    private string emptyPasswordField = "Password field is mandatory.";
    private string minimum7CharacterRequired = "Minimum 7 characters are required in password.";
    private string minimum6CharacterRequired = "6 characters are required in password.";
    private string emptyVerificationCodeField = "Verification code field is mandatory.";
    private string nonMatchingPasswords = "Passwords do not match.";
    private string emptyConfirmPassword = "Confirm password field is empty.";
    private string avatarAppearanceNotSelected = "Avatar appearance is mandatory.";
    private string emptyAllInputFields = "All input fields are mandatory.";

    private int passwordCharacterCount = 7;
    private int verificationCodeCharacterCount = 6;

    private void Awake()
    {
        Instance = this;
    }

    public void EnableRegistrationPage()
    {
        StateMachineHandler.Instance.UserAuthenticationStateMachineHandler(StateMachineHandler.USER_AUTHENTICATION_STATE.REGISTRATION_PAGE_1);
    }

    public void EnableForgotPasswordPage()
    {
        StateMachineHandler.Instance.UserAuthenticationStateMachineHandler(StateMachineHandler.USER_AUTHENTICATION_STATE.FORGOT_PASSWORD_PAGE);
    }

    public void SubmitUserLoginDetails()
    {
        if (!CheckEmptyInputField(loginEmailInputField))
        {
            if (IsEmail(loginEmailInputField.text))
            {
                if (!CheckEmptyInputField(loginPasswordInputField))
                {
                    if (loginPasswordInputField.text.Length >= passwordCharacterCount)
                    {
                        LoginManager.Instance.LoginUser(loginEmailInputField.text, loginPasswordInputField.text);
                    }
                    else
                    {
                        DisplayErrorText(minimum7CharacterRequired);
                    }
                }
                else
                {
                    DisplayErrorText(emptyPasswordField);
                }
            }
            else
            {
                DisplayErrorText(invalidEmail);
            }
        }
        else
        {
            DisplayErrorText(emptyEmailField);
        }
    }

    public void SelectGender(string gender)
    {
        registrationUserAvatarAppearance = gender;

        if (gender == "Male")
        {
            genderSelectionMaleSelectedImage.SetActive(true);
            genderSelectionFemaleSelectedImage.SetActive(false);
        }
        else
        {
            genderSelectionMaleSelectedImage.SetActive(false);
            genderSelectionFemaleSelectedImage.SetActive(true);
        }
    }

    public void SubmitRegistrationPage1Details()
    {
        if (!CheckEmptyInputField(registrationNameInputField))
        {
            if (!CheckEmptyInputField(registrationEmailInputField))
            {
                if (IsEmail(registrationEmailInputField.text))
                {
                    if (!CheckEmptyInputField(registrationPasswordInputField))
                    {
                        if (registrationPasswordInputField.text.Length >= passwordCharacterCount)
                        {
                            if (!CheckEmptyInputField(registrationConfirmPasswordInputField))
                            {
                                if (registrationPasswordInputField.text == registrationConfirmPasswordInputField.text)
                                {
                                    if (registrationUserAvatarAppearance.Length != 0)
                                    {
                                        registrationUserName = registrationNameInputField.text;
                                        registrationUserEmail = registrationEmailInputField.text;
                                        registrationUserPassword = registrationPasswordInputField.text;

                                        StateMachineHandler.Instance.UserAuthenticationStateMachineHandler(StateMachineHandler.USER_AUTHENTICATION_STATE.REGISTRATION_PAGE_2);
                                    }
                                    else
                                    {
                                        DisplayErrorText(avatarAppearanceNotSelected);
                                    }
                                }
                                else
                                {
                                    DisplayErrorText(nonMatchingPasswords);
                                }
                            }
                            else
                            {
                                DisplayErrorText(emptyConfirmPassword);
                            }
                        }
                        else
                        {
                            DisplayErrorText(minimum7CharacterRequired);
                        }
                    }
                    else
                    {
                        DisplayErrorText(emptyPasswordField);
                    }
                }
                else
                {
                    DisplayErrorText(invalidEmail);
                }
            }
            else
            {
                DisplayErrorText(emptyEmailField);
            }
        }
        else
        {
            DisplayErrorText(emptyNameField);
        }
    }

    public void SubmitRegistrationDetails()
    {
        LoginManager.Instance.RegisterUserBeforeToken(registrationUserName, registrationUserEmail, registrationCompanyNameInputField.text, registrationDivisionNameInputField.text, registrationTitleInputField.text, registrationContactNumberInputField.text, registrationUserPassword, registrationUserAvatarAppearance, registrationUserPassword);
    }

    public void SubmitRegistrationVerificationCode()
    {
        if (!CheckEmptyInputField(verificationCodeInputField))
        {
            if (verificationCodeInputField.text.Length == verificationCodeCharacterCount)
            {
                LoginManager.Instance.RegisterUserAfterToken(verificationCodeInputField.text);
            }
            else
            {
                // 6 characters required in verification code.
                DisplayErrorText(minimum6CharacterRequired);
            }
        }
        else
        {
            // Verification code input field is empty.
            DisplayErrorText(emptyVerificationCodeField);
        }
    }

    public void SubmitForgotPasswordEmail()
    {
        if (!CheckEmptyInputField(forgotPasswordEmailInputField))
        {
            if (IsEmail(forgotPasswordEmailInputField.text))
            {
                changePasswordEmail = forgotPasswordEmailInputField.text;
                LoginManager.Instance.ForgotPasswordBeforeCode(forgotPasswordEmailInputField.text);
            }
            else
            {
                // Invalid email.
                DisplayErrorText(invalidEmail);
            }
        }
        else
        {
            // Forgot password email input field is empty.
            DisplayErrorText(emptyEmailField);
        }
    }

    public void SubmitChangePasswordDetails()
    {
        if (!CheckEmptyInputField(changePasswordCodeInputField))
        {
            if (changePasswordCodeInputField.text.Length == verificationCodeCharacterCount)
            {
                if (!CheckEmptyInputField(changePasswordCodeInputField))
                {
                    if(changePasswordNewPasswordInputField.text.Length >= passwordCharacterCount)
                    {
                        if(!CheckEmptyInputField(changePasswordConfirmPasswordInputField))
                        {
                            if(changePasswordNewPasswordInputField.text == changePasswordConfirmPasswordInputField.text)
                            {
                                LoginManager.Instance.ForgotPasswordAfterCode(changePasswordEmail, changePasswordCodeInputField.text, changePasswordNewPasswordInputField.text);
                            }
                            else
                            {
                                DisplayErrorText(nonMatchingPasswords);
                            }
                        }
                        else
                        {
                            DisplayErrorText(emptyConfirmPassword);
                        }
                    }
                    else
                    {
                        DisplayErrorText(minimum7CharacterRequired);
                    }
                }
                else
                {
                    DisplayErrorText(emptyPasswordField);
                }
            }
            else
            {
                DisplayErrorText(minimum6CharacterRequired);
            }
        }
        else
        {
            DisplayErrorText(emptyVerificationCodeField);
        }
    }


    public void ResetPages()
    {
        loginEmailInputField.text = "";
        loginPasswordInputField.text = "";
        verificationCodeInputField.text = "";
        forgotPasswordEmailInputField.text = "";
        changePasswordCodeInputField.text = "";
        changePasswordNewPasswordInputField.text = "";
        changePasswordConfirmPasswordInputField.text = "";
    }


    #region Error text 

    public void DisplayErrorText(string error)
    {
        errorText.text = error;
        errorTextParentComponent.SetActive(true);
        errorTextBGImage.DOFade(1f, 0.25f);
        errorText.DOFade(1f, 0.25f);

        StartCoroutine(DisableErrorText());
    }

    public IEnumerator DisableErrorText()
    {
        yield return new WaitForSeconds(2f);
        errorTextBGImage.DOFade(0f, 0.25f);
        errorText.DOFade(0f, 0.25f);
        yield return new WaitForSeconds(0.25f);
        errorTextParentComponent.SetActive(false);
    }

    #endregion

    #region Validators

    public bool CheckEmptyInputField(TMP_InputField inputField)
    {
        if (inputField.text == "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public const string MatchEmailPattern =
            @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
              + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";


    /// <summary>
    /// Checks whether the given Email-Parameter is a valid E-Mail address.
    /// </summary>
    /// <param name="email">Parameter-string that contains an E-Mail address.</param>
    /// <returns>True, wenn Parameter-string is not null and contains a valid E-Mail address;
    /// otherwise false.</returns>
    public static bool IsEmail(string email)
    {
        if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
        else return false;
    }
    #endregion
}