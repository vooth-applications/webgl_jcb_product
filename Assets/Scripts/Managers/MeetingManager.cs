﻿using DG.Tweening;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeetingManager : MonoBehaviour
{
    public static MeetingManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    [Header("Meeting UI panel references")]
    public GameObject sendMeetingInvitationPanel;
    public GameObject recieveMeetingInvitationPanel;

    public List<GameObject> listOfChairs;
    public bool meeting;
    public bool modelsOnTable;

    public GameObject meetingPosition;
    public int attendeeCountValue = 0;

    public void StartMeeting()
    {
        // Invite all for the meeting and start the meeting.

        Debug.Log("StartMeeting is called");

        StartCoroutine(JoinMeeting());
        PhotonNetworkManager.Instance.SendMeetingInvitation();
    }

    public void CloseMeeting()
    {
        // Make everyone leave the meeting and close the meeting.

        LeaveMeeting();
        PhotonNetworkManager.Instance.SendActionToLeaveMeeting();
    }

    public void JoinMeetingManually()
    {
        StartCoroutine(JoinMeeting());
    }

    public IEnumerator JoinMeeting()
    {
        // Join meeting individually.

        Debug.Log("JoinMeeting is called");

        sendMeetingInvitationPanel.SetActive(false);

        StartCoroutine(UserPositionManager.Instance.GetMeetingChairsValue());

        yield return new WaitUntil(() => UserPositionManager.Instance.meetingChairsValueUpdated);

        if (meetingPosition == null)
        {
            Debug.Log("Meeting position is null because all chairs are taken");
            yield return null;
        }

        PhotonNetworkManager.Instance.localPlayer.GetComponent<MovementController>().meetingPosition = meetingPosition;

        if (AppManager.Instance.thisAppController == AppManager.Controller.HOST)
        {
            PhotonNetworkManager.Instance.localPlayer.transform.DOMove(meetingPosition.GetComponent<ChairProperties>().chairSittingPosition, 1f);
            PhotonNetworkManager.Instance.localPlayer.transform.DORotate(new Vector3(0f, -90f, 0f), 1f);
        }
        else
        {
            PhotonNetworkManager.Instance.localPlayer.transform.DOMove(meetingPosition.GetComponent<ChairProperties>().chairSittingPosition, 1f);
            PhotonNetworkManager.Instance.localPlayer.transform.DORotate(new Vector3(0f, 90f, 0f), 1f);
        }

        meeting = true;

        UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().ToggleMeetingUI();
        UIHandler.Instance.visitorBottomBarUIManager.GetComponent<VisitorBottomBarUIManager>().ToggleMeetingUI();

        NickNameProperties nickNameProperties = new NickNameProperties
        {
            userName = LoginManager.Instance.localUserDetails.name,
            userRole = AppManager.Instance.thisAppController == AppManager.Controller.HOST ? "HOST" : "VISITOR",
            userOccupyiedChairName = meetingPosition.name
        };

        string nickNamePropertiesJson = JsonUtility.ToJson(nickNameProperties);
        Debug.Log("nickNamePropertiesJson : " + nickNamePropertiesJson);
        PhotonNetwork.LocalPlayer.NickName = nickNamePropertiesJson;
        Debug.Log("PhotonNetwork.LocalPlayer.NickName : " + PhotonNetwork.LocalPlayer.NickName);

        IncreaseMeetingAttendeeCount();
        StartCoroutine(AudioVideoManager.Instance.SwitchToVideoChat());
    }
    public void LeaveMeeting()
    {
        // Leave meeting individually.

        if (modelsOnTable)
        {
            CallProductsFromTableToVooth(false);
            UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().callProductsButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
        }

        if (AppManager.Instance.hostPresentationPanel.activeInHierarchy)
        {
            AppManager.Instance.hostPresentationPanel.SetActive(false);
            UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().showPresentationButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
        }

        if (AppManager.Instance.videoPanel.activeInHierarchy)
        {
            AppManager.Instance.ToggleVideoPanel(true);
            UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().showVideoButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
        }

        PhotonNetworkManager.Instance.localPlayer.GetComponent<MovementController>().meetingPosition.GetComponent<ChairProperties>().chairTaken = false;
        UserPositionManager.Instance.SetMeetingChairsValue();

        if (AppManager.Instance.thisAppController == AppManager.Controller.HOST)
        {
            PhotonNetworkManager.Instance.localPlayer.transform.DOMove(new Vector3(PhotonNetworkManager.Instance.localPlayer.transform.localPosition.x + 10f, PhotonNetworkManager.Instance.localPlayer.transform.localPosition.y + 2.5f, PhotonNetworkManager.Instance.localPlayer.transform.localPosition.z), 1f);
        }
        else
        {
            PhotonNetworkManager.Instance.localPlayer.transform.DOMove(new Vector3(PhotonNetworkManager.Instance.localPlayer.transform.localPosition.x - 10f, PhotonNetworkManager.Instance.localPlayer.transform.localPosition.y + 2.5f, PhotonNetworkManager.Instance.localPlayer.transform.localPosition.z), 1f);
        }
        meeting = false;
        UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().ToggleMeetingUI();
        UIHandler.Instance.visitorBottomBarUIManager.GetComponent<VisitorBottomBarUIManager>().ToggleMeetingUI();

        DecreaseMeetingAttendeeCount();
        StartCoroutine(AudioVideoManager.Instance.SwitchToAudioChat());
    }

    public void IncreaseMeetingAttendeeCount()
    {
        attendeeCountValue = (int)PhotonNetwork.CurrentRoom.CustomProperties[UserPositionManager.Instance.transformReferencer.ATTENDEECOUNT];

        attendeeCountValue++;

        UserPositionManager.Instance.seatProperties[UserPositionManager.Instance.transformReferencer.ATTENDEECOUNT] = attendeeCountValue;
        PhotonNetwork.CurrentRoom.SetCustomProperties(UserPositionManager.Instance.seatProperties);
    }

    public void DecreaseMeetingAttendeeCount()
    {
        attendeeCountValue = (int)PhotonNetwork.CurrentRoom.CustomProperties[UserPositionManager.Instance.transformReferencer.ATTENDEECOUNT];

        attendeeCountValue--;

        UserPositionManager.Instance.seatProperties[UserPositionManager.Instance.transformReferencer.ATTENDEECOUNT] = attendeeCountValue;
        PhotonNetwork.CurrentRoom.SetCustomProperties(UserPositionManager.Instance.seatProperties);
    }

    public void RecieveMeetingInvitation()
    {
        recieveMeetingInvitationPanel.SetActive(true);
    }

    public void AcceptMeetingInvitation()
    {
        recieveMeetingInvitationPanel.SetActive(false);
        StartCoroutine(JoinMeeting());
    }

    public void DeclineMeetingInvitation()
    {
        recieveMeetingInvitationPanel.SetActive(false);
        meeting = false;
    }

    #region Meeting functionalities

    public void CallProductsToggle()
    {
        if (modelsOnTable)
        {
            CallProductsFromTableToVooth(true);
        }
        else
        {
            CallProductsFromVoothToTable(true);
        }
    }

    public void CallProductsFromVoothToTable(bool sendOverNetwork)
    {
        if (AppManager.Instance.hostPresentationPanel.activeInHierarchy)
        {
            AppManager.Instance.hostPresentationPanel.SetActive(false);
            UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().showPresentationButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
        }

        if (AppManager.Instance.videoPanel.activeInHierarchy)
        {
            AppManager.Instance.ToggleVideoPanel(true);
            UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().showVideoButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
        }


        foreach (GameObject model in AppManager.Instance.listOfActiveModels)
        {
            model.transform.DOMove(model.GetComponent<ModelTweener>().midTweenPosition.transform.localPosition, 0.65f).OnComplete(() => SendProductsFromMidTweenToTable(model));
            model.transform.DORotate(new Vector3(0f, 45f, 0f), 0.65f);
        }

        if (sendOverNetwork)
        {
            PhotonNetworkManager.Instance.SendCPFromVToT();
        }
    }

    public void SendProductsFromMidTweenToTable(GameObject model)
    {
        model.transform.DOMove(model.GetComponent<ModelTweener>().meetingTablePosition.transform.localPosition, 0.65f);
        model.transform.DORotate(Vector3.zero, 0.65f);
        model.transform.DOScale(model.GetComponent<ModelTweener>().meetingTablePosition.transform.localScale, 0.65f);
        modelsOnTable = true;
    }

    public void CallProductsFromTableToVooth(bool sendOverNetwork)
    {
        foreach (GameObject model in AppManager.Instance.listOfActiveModels)
        {
            model.transform.DOMove(model.GetComponent<ModelTweener>().midTweenPosition.transform.localPosition, 0.65f).OnComplete(() => SendProductsFromTableToMidTween(model));
            model.transform.DORotate(new Vector3(0f, 45f, 0f), 0.65f);
        }

        if (sendOverNetwork)
        {
            PhotonNetworkManager.Instance.SendCPFromTToV();
        }
    }

    public void SendProductsFromTableToMidTween(GameObject model)
    {
        model.transform.DOMove(model.GetComponent<ModelTweener>().defaultPosition.transform.localPosition, 0.65f);
        model.transform.DORotate(model.GetComponent<ModelTweener>().defaultPosition.transform.localRotation.eulerAngles, 0.65f);
        model.transform.DOScale(model.GetComponent<ModelTweener>().defaultPosition.transform.localScale, 0.65f);
        modelsOnTable = false;
    }

    #endregion
}