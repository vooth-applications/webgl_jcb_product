﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerOld : MonoBehaviour
{
    public static UIManagerOld Instance;

    private void Awake()
    {
        Instance = this;
    }

    public GameObject videoPanel;

    [Header("Host UI panel references")]
    public GameObject hostPresentationPanel;

    [Header("Visitor UI panel references")]
    public GameObject visitorPresentationPanel;

    [Header("Host UI button references")]
    public GameObject hostMicOnButton;
    public GameObject hostMicOffButton;
    public GameObject hostWatchPresentationButton;
    public GameObject hostClosePresentationButton;
    public GameObject hostWatchVideoButton;
    public GameObject hostCloseVideoButton;
    public GameObject hostAnimateModelsButton;
    public GameObject hostStopModelsButton;
    public GameObject hostCallProductsButton;
    public GameObject hostRemoveProductsButton;
    public GameObject hostStartMeetingButton;
    public GameObject hostJoinMeetingButton;
    public GameObject hostCloseMeetingButton;
    public GameObject hostLeaveMeetingButton;
    public GameObject settleOnTableHighlightImage;

    [Header("Visitor UI button references")]
    public GameObject visitorMicOnButton;
    public GameObject visitorMicOffButton;
    public GameObject visitorWatchVideoButton;
    public GameObject visitorCloseVideoButton;
    public GameObject visitorAnimateModelsButton;
    public GameObject visitorStopModelsButton;
    public GameObject visitorJoinMeetingButton;
    public GameObject visitorLeaveMeetingButton;

    [Header("Error text components")]
    public GameObject errorTextParentComponent;
    public Image errorTextBGImage;
    public TextMeshProUGUI errorText;
    public bool errorShown;

    //private void FixedUpdate()
    //{
    //    if(!MeetingManager.Instance.meeting)
    //    {
    //        if(!AppManager.Instance.animatingModels)
    //        {
    //            visitorAnimateModelsButton.SetActive(true);
    //        }
    //    }
    //}

    public void HandlePresentationToggle()
    {
        if (hostPresentationPanel.activeInHierarchy)
        {
            hostWatchPresentationButton.SetActive(true);
            hostClosePresentationButton.SetActive(false);

            hostPresentationPanel.SetActive(false);
        }
        else
        {
            hostWatchPresentationButton.SetActive(false);
            hostClosePresentationButton.SetActive(true);

            hostPresentationPanel.SetActive(true);
        }

        PhotonNetworkManager.Instance.SendPresentationToggle();
    }

    public void HandleVideoToggle()
    {
        if (videoPanel.activeInHierarchy)
        {
            videoPanel.transform.GetChild(0).GetComponent<RawImage>().DOFade(0f, 0.25f);
            videoPanel.transform.GetChild(0).GetComponent<Video_Start_Stop>().OnPointerClick();
            videoPanel.transform.DOScale(Vector3.zero, 0.25f).OnComplete(DisableVideoPanel);

            if (AppManager.Instance.thisAppController == AppManager.Controller.HOST)
            {
                hostWatchVideoButton.SetActive(true);
                hostCloseVideoButton.SetActive(false);
            }
            else
            {
                visitorWatchVideoButton.SetActive(true);
                visitorCloseVideoButton.SetActive(false);
            }
        }
        else
        {
            videoPanel.SetActive(true);
            videoPanel.transform.DOScale(Vector3.one, 0.25f);
            videoPanel.transform.GetChild(0).GetComponent<RawImage>().DOFade(1f, 0.25f);
            videoPanel.transform.GetChild(0).GetComponent<Video_Start_Stop>().OnPointerClick();

            if (AppManager.Instance.thisAppController == AppManager.Controller.HOST)
            {
                hostWatchVideoButton.SetActive(false);
                hostCloseVideoButton.SetActive(true);
            }
            else
            {
                visitorWatchVideoButton.SetActive(false);
                visitorCloseVideoButton.SetActive(true);
            }
        }
    }

    public void HandleCallProductsToggle()
    {
        if(AppManager.Instance.callProducts)
        {
            hostCallProductsButton.SetActive(true);
            hostRemoveProductsButton.SetActive(false);

            MeetingManager.Instance.CallProductsFromTableToVooth(true);
            AppManager.Instance.callProducts = false;
        }
        else
        {
            hostCallProductsButton.SetActive(false);
            hostRemoveProductsButton.SetActive(true);

            MeetingManager.Instance.CallProductsFromVoothToTable(true);
            AppManager.Instance.callProducts = true;
        }
    }

    public void DisableVideoPanel()
    {
        videoPanel.SetActive(false);
    }

    #region Error text 

    public IEnumerator DisplayerErrorTextAfterDelay(string error)
    {
        yield return new WaitForSeconds(2f);
        if (!errorShown)
        {
            DisplayErrorText(error);
        }
        else
        {
            StartCoroutine(DisplayerErrorTextAfterDelay(error));
        }
    }

    public void DisplayErrorText(string error)
    {
        errorShown = true;
        errorText.text = error;
        errorTextParentComponent.SetActive(true);
        errorTextBGImage.DOFade(1f, 0.25f);
        errorText.DOFade(1f, 0.25f);

        StartCoroutine(DisableErrorText());
    }

    public IEnumerator DisableErrorText()
    {
        yield return new WaitForSeconds(2f);
        errorTextBGImage.DOFade(0f, 0.25f);
        errorText.DOFade(0f, 0.25f);
        yield return new WaitForSeconds(0.25f);
        errorTextParentComponent.SetActive(false);
        errorShown = false;
    }
    #endregion
}