﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class UserPositionManager : MonoBehaviour
{
    public static UserPositionManager Instance;
    public TransformReferencer transformReferencer;
    public ExitGames.Client.Photon.Hashtable seatProperties;
    public ExitGames.Client.Photon.Hashtable chairProperties;

    public bool meetingChairsDefaultValueUpdated;
    public bool meetingChairsValueUpdated;

    public ChairProperties H1;
    public ChairProperties H2;
    public ChairProperties H3;
    public ChairProperties V1;
    public ChairProperties V2;
    public ChairProperties V3;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        seatProperties = new ExitGames.Client.Photon.Hashtable();
        chairProperties = new ExitGames.Client.Photon.Hashtable();
    }

    #region Position Checks

    public GameObject EmptyPositionCheckInitial()
    {
        if (AppManager.Instance.thisAppController == AppManager.Controller.HOST)
        {
            if (!ReturnSeatTakenValue(transformReferencer.HOST1IT))
            {
                SetSeatTakenValue(transformReferencer.HOST1IT, true);
                return transformReferencer.host1InitialTransform;
            }
            else if (!ReturnSeatTakenValue(transformReferencer.HOST2IT))
            {
                SetSeatTakenValue(transformReferencer.HOST2IT, true);
                return transformReferencer.host2InitialTransform;
            }
            else if (!ReturnSeatTakenValue(transformReferencer.HOST3IT))
            {
                SetSeatTakenValue(transformReferencer.HOST3IT, true);
                return transformReferencer.host3InitialTransform;
            }
            else if (!ReturnSeatTakenValue(transformReferencer.VISITOR1IT))
            {
                SetSeatTakenValue(transformReferencer.VISITOR1IT, true);
                return transformReferencer.visitor1InitialTransform;
            }
            else if (!ReturnSeatTakenValue(transformReferencer.VISITOR2IT))
            {
                SetSeatTakenValue(transformReferencer.VISITOR2IT, true);
                return transformReferencer.visitor2InitialTransform;
            }
            else if (!ReturnSeatTakenValue(transformReferencer.VISITOR3IT))
            {
                SetSeatTakenValue(transformReferencer.VISITOR2IT, true);
                return transformReferencer.visitor3InitialTransform;
            }
        }
        else
        {
            if (!ReturnSeatTakenValue(transformReferencer.VISITOR1IT))
            {
                SetSeatTakenValue(transformReferencer.VISITOR1IT, true);
                return transformReferencer.visitor1InitialTransform;
            }
            else if (!ReturnSeatTakenValue(transformReferencer.VISITOR2IT))
            {
                SetSeatTakenValue(transformReferencer.VISITOR2IT, true);
                return transformReferencer.visitor2InitialTransform;
            }
            else if (!ReturnSeatTakenValue(transformReferencer.VISITOR3IT))
            {
                SetSeatTakenValue(transformReferencer.VISITOR2IT, true);
                return transformReferencer.visitor3InitialTransform;
            }
            else if (!ReturnSeatTakenValue(transformReferencer.HOST1IT))
            {
                SetSeatTakenValue(transformReferencer.HOST1IT, true);
                return transformReferencer.host1InitialTransform;
            }
            else if (!ReturnSeatTakenValue(transformReferencer.HOST2IT))
            {
                SetSeatTakenValue(transformReferencer.HOST2IT, true);
                return transformReferencer.host2InitialTransform;
            }
            else if (!ReturnSeatTakenValue(transformReferencer.HOST3IT))
            {
                SetSeatTakenValue(transformReferencer.HOST3IT, true);
                return transformReferencer.host3InitialTransform;
            }
        }
        return null;
    }

    public void SetDefaultSeatTakenValuesInitial()
    {
        SetSeatTakenValue(transformReferencer.HOST1IT, false);
        SetSeatTakenValue(transformReferencer.HOST2IT, false);
        SetSeatTakenValue(transformReferencer.HOST3IT, false);
        SetSeatTakenValue(transformReferencer.VISITOR1IT, false);
        SetSeatTakenValue(transformReferencer.VISITOR2IT, false);
        SetSeatTakenValue(transformReferencer.VISITOR3IT, false);

        seatProperties[transformReferencer.ATTENDEECOUNT] = 0;
        PhotonNetwork.CurrentRoom.SetCustomProperties(seatProperties);
    }

    public void SetSeatTakenValue(string positionName, bool value)
    {
        seatProperties[positionName] = value;
        PhotonNetwork.CurrentRoom.SetCustomProperties(seatProperties);
    }
    public bool ReturnSeatTakenValue(string positionName)
    {
        Debug.Log("Checking ReturnSeatTakenValue with position : " + positionName);

        bool positionValue = (bool)PhotonNetwork.CurrentRoom.CustomProperties[positionName];
        return positionValue;
    }

    #endregion

    #region New Position Checks

    //public GameObject CheckAvailableMeetingChairs()
    //{
    //    if(AppManager.Instance.thisAppController == AppManager.Controller.HOST)
    //    {
    //        CheckMeetingChairsForHost();
    //    }
    //    else
    //    {
    //        CheckMeetingChairsForVisitor();
    //    }
    //}

    public void SetChairDefaultValuesForMeeting()
    {
        foreach (GameObject chair in MeetingManager.Instance.listOfChairs)
        {
            SetChairTakenValue(chair.name, false);
        }

        Debug.Log("Chair taken default values set");
    }

    public IEnumerator GetMeetingChairsValue()
    {
        meetingChairsDefaultValueUpdated = false;

        foreach(GameObject chair in MeetingManager.Instance.listOfChairs)
        {
            chair.GetComponent<ChairProperties>().chairTaken = (bool)PhotonNetwork.CurrentRoom.CustomProperties[chair.name];
        }

        yield return null;

        Debug.Log("Chair taken values fetched");

        foreach (GameObject chair in MeetingManager.Instance.listOfChairs)
        {
            Debug.Log("Current Chair : " + chair.name + ", ChairTakenValue : " + chair.GetComponent<ChairProperties>().chairTaken);
        }

        if (AppManager.Instance.thisAppController == AppManager.Controller.HOST)
        {
            MeetingManager.Instance.meetingPosition = CheckMeetingChairsForHost();
        }
        else
        {
            MeetingManager.Instance.meetingPosition = CheckMeetingChairsForVisitor();
        }

        yield return new WaitUntil(() => meetingChairsValueUpdated);

        meetingChairsDefaultValueUpdated = true;
        SetMeetingChairsValue();
    }

    public void SetMeetingChairsValue()
    {
        foreach (GameObject chair in MeetingManager.Instance.listOfChairs)
        {
            SetChairTakenValue(chair.name, chair.GetComponent<ChairProperties>().chairTaken);
        }

        Debug.Log("Chair taken updated values set");

        foreach (GameObject chair in MeetingManager.Instance.listOfChairs)
        {
            Debug.Log("Updated Chair : " + chair.name + ", ChairTakenValue : " + chair.GetComponent<ChairProperties>().chairTaken);
        }
    }

    public GameObject CheckMeetingChairsForHost()
    {
        meetingChairsValueUpdated = false;
        if (H1.chairTaken)
        {
            if (H2.chairTaken)
            {
                if (H3.chairTaken)
                {
                    if (V2.chairTaken)
                    {
                        if (V3.chairTaken)
                        {
                            Debug.Log("All meeting chairs are taken");
                            meetingChairsValueUpdated = false;
                            return null;
                        }
                        else
                        {
                            Debug.Log("V3 is not taken");
                            meetingChairsValueUpdated = true;
                            V3.GetComponent<ChairProperties>().chairTaken = true;
                            return V3.gameObject;
                        }
                    }
                    else
                    {
                        Debug.Log("V2 is not taken");
                        meetingChairsValueUpdated = true;
                        V2.GetComponent<ChairProperties>().chairTaken = true;
                        return V2.gameObject;
                    }
                }
                else
                {
                    Debug.Log("H3 is not taken");
                    meetingChairsValueUpdated = true;
                    H3.GetComponent<ChairProperties>().chairTaken = true;
                    return H3.gameObject;
                }
            }
            else
            {
                Debug.Log("H2 is not taken");
                meetingChairsValueUpdated = true;
                H2.GetComponent<ChairProperties>().chairTaken = true;
                return H2.gameObject;
            }
        }
        else
        {
            Debug.Log("H1 is not taken");
            meetingChairsValueUpdated = true;
            H1.GetComponent<ChairProperties>().chairTaken = true;
            return H1.gameObject;
        }
    }

    public GameObject CheckMeetingChairsForVisitor()
    {
        meetingChairsValueUpdated = false;
        if (V1.chairTaken)
        {
            if (V2.chairTaken)
            {
                if (V3.chairTaken)
                {
                    if (H2.chairTaken)
                    {
                        if (H3.chairTaken)
                        {
                            Debug.Log("All meeting chairs are taken");
                            meetingChairsValueUpdated = false;
                            return null;
                        }
                        else
                        {
                            Debug.Log("H3 is not taken");
                            meetingChairsValueUpdated = true;
                            H3.GetComponent<ChairProperties>().chairTaken = true;
                            return H3.gameObject;
                        }
                    }
                    else
                    {
                        Debug.Log("H2 is not taken");
                        meetingChairsValueUpdated = true;
                        H2.GetComponent<ChairProperties>().chairTaken = true;
                        return H2.gameObject;
                    }
                }
                else
                {
                    Debug.Log("V3 is not taken");
                    meetingChairsValueUpdated = true;
                    V3.GetComponent<ChairProperties>().chairTaken = true;
                    return V3.gameObject;
                }
            }
            else
            {
                Debug.Log("V2 is not taken");
                meetingChairsValueUpdated = true;
                V2.GetComponent<ChairProperties>().chairTaken = true;
                return V2.gameObject;
            }
        }
        else
        {
            Debug.Log("V1 is not taken");
            meetingChairsValueUpdated = true;
            V1.GetComponent<ChairProperties>().chairTaken = true;
            return V1.gameObject;
        }
    }

    public void SetChairTakenValue(string chairName, bool value)
    {
        chairProperties[chairName] = value;
        PhotonNetwork.CurrentRoom.SetCustomProperties(chairProperties);
    }

    public bool GetChairTakenValue(string chairName)
    {
        Debug.Log("Checking GetChairTakenValue with chair name : " + chairName);

        bool positionValue = (bool)PhotonNetwork.CurrentRoom.CustomProperties[chairName];
        return positionValue;
    }

    #endregion
}