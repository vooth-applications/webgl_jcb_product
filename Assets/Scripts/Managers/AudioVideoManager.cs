﻿using Byn.Unity.Examples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioVideoManager : MonoBehaviour
{
    public static AudioVideoManager Instance;

    public ConferenceApp conferenceApp;

    private void Awake()
    {
        Instance = this;
    }

    public void StartAudioChat()
    {
        conferenceApp.AudioToggle(true);
        conferenceApp.VideoToggle(false);

        conferenceApp.JoinButtonPressed();
    }

    public void StartAudioVideoChat()
    {
        conferenceApp.AudioToggle(true);
        conferenceApp.VideoToggle(true);

        conferenceApp.JoinButtonPressed();
    }

    public IEnumerator SwitchToAudioChat()
    {
        conferenceApp.ShutdownButtonPressed();
        yield return new WaitForSeconds(1f);

        StartAudioChat();
    }

    public IEnumerator SwitchToVideoChat()
    {
        conferenceApp.ShutdownButtonPressed();
        yield return new WaitForSeconds(1f);

        StartAudioVideoChat();
    }

    public void Mute()
    {
       AppManager.Instance.videoConferencePrefab.GetComponent<ConferenceApp>().mCall.SetMute(true);

        if (AppManager.Instance.thisAppController == AppManager.Controller.HOST)
        {
            UIManagerOld.Instance.hostMicOnButton.SetActive(false);
            UIManagerOld.Instance.hostMicOffButton.SetActive(true);
        }
        else
        {
            UIManagerOld.Instance.visitorMicOnButton.SetActive(false);
            UIManagerOld.Instance.visitorMicOffButton.SetActive(true);
        }
    }

    public void Unmute()
    {
        AppManager.Instance.videoConferencePrefab.GetComponent<ConferenceApp>().mCall.SetMute(false);

        if (AppManager.Instance.thisAppController == AppManager.Controller.HOST)
        {
            UIManagerOld.Instance.hostMicOnButton.SetActive(true);
            UIManagerOld.Instance.hostMicOffButton.SetActive(false);
        }
        else
        {
            UIManagerOld.Instance.visitorMicOnButton.SetActive(true);
            UIManagerOld.Instance.visitorMicOffButton.SetActive(false);
        }
    }
}