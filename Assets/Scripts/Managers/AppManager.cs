﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppManager : MonoBehaviour
{
    public static AppManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public enum Controller
    {
        HOST,
        VISITOR
    }

    [Header("Main UI panel references")]
    public GameObject hostUI;
    public GameObject visitorUI;
    public GameObject joystick;

    public GameObject videoConferencePrefab;

    public Controller thisAppController;
    public GameObject userSelectionPanel;

    public bool callProducts;
    public List<GameObject> listOfActiveModels;
    public GameObject hostMeetingModelsButtonsPanel;
    public GameObject hostNonMeetingModelsButtonsPanel;

    [Header("Models transform references")]
    public GameObject model1VoothTransform;
    public GameObject model1TableTransform;
    public GameObject model2VoothTransform;
    public GameObject model2TableTransform;

    public List<GameObject> listOfMaleAvatars;
    public List<GameObject> listOfFemaleAvatars;
    public int playerIndex;

    [Header("Main video display panel")]
    public GameObject videoPanel;

    [Header("Host UI panel references")]
    public GameObject hostPresentationPanel;

    [Header("Visitor UI panel references")]
    public GameObject visitorPresentationPanel;

    [Header("Invitation panel references")]
    public GameObject sendMeetingInvitationPanel;
    public GameObject recieveMeetingInvitationPanel;

    private void Start()
    {
        StateMachineHandler.Instance.AppStateHandler(StateMachineHandler.APP_STATE.USER_AUTHENTICATION);
    }

    public void SelectUserType(int index)
    {
        userSelectionPanel.SetActive(false);

        if (index == 0)
        {
            thisAppController = Controller.HOST;
        }
        else
        {
            thisAppController = Controller.VISITOR;
        }

        if (LoginManager.Instance.localUserDetails.avatarAppearance == "Male")
        {
            PhotonNetworkManager.thisPlayersAppearance = PhotonNetworkManager.APPEARANCE.MAN;
        }
        else
        {
            PhotonNetworkManager.thisPlayersAppearance = PhotonNetworkManager.APPEARANCE.WOMAN;
        }

        PhotonNetworkManager.Instance.StartPUNConnection();
    }

    public void ToggleVideoPanel(bool value)
    {
        if (value)
        {
            videoPanel.transform.GetChild(0).GetComponent<RawImage>().DOFade(0f, 0.25f);
            videoPanel.transform.GetChild(0).GetComponent<Video_Start_Stop>().OnPointerClick();
            videoPanel.transform.GetChild(0).GetComponent<Video_Start_Stop>().SetCloseButtonState(false);
            videoPanel.transform.DOScale(Vector3.zero, 0.25f).OnComplete(DisableVideoPanelOnCompletion);
        }
        else
        {
            videoPanel.SetActive(true);
            videoPanel.transform.DOScale(Vector3.one, 0.25f);
            videoPanel.transform.GetChild(0).GetComponent<RawImage>().DOFade(1f, 0.25f);
            videoPanel.transform.GetChild(0).GetComponent<Video_Start_Stop>().OnPointerClick();
            videoPanel.transform.GetChild(0).GetComponent<Video_Start_Stop>().CheckVideoStatus();
        }
    }

    public void DisableVideoPanelOnCompletion()
    {
        videoPanel.SetActive(false);
    }
}