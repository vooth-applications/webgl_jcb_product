﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Video_Start_Stop : MonoBehaviour
{
    public VideoPlayer mPlayer;
    public GameObject closeButton;

    public void OnPointerClick()
    {
        if (!mPlayer.isPlaying)
        {
            mPlayer.Play();
        }
        else
        {
            mPlayer.Pause();
        }
    }

    public void CheckVideoStatus()
    {
        if (mPlayer.isPrepared)
        {
            SetCloseButtonState(true);
        }
        else
        {
            Invoke("CheckVideoStatus", 0.2f);
        }
    }

    public void SetCloseButtonState(bool state)
    {
        closeButton.SetActive(state);
    }

    public void CloseButtonVideoToggle()
    {
        if (AppManager.Instance.thisAppController == AppManager.Controller.HOST)
        {
            if (MeetingManager.Instance.meeting)
            {
                UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().showVideoButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
            }
            else
            {
                UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().watchVideoButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
            }

            UIHandler.Instance.hostBottomBarUIManager.VideoToggle(false);
        }
        else
        {
            if (!MeetingManager.Instance.meeting)
            {
                UIHandler.Instance.visitorBottomBarUIManager.GetComponent<VisitorBottomBarUIManager>().watchVideoButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
            }

            UIHandler.Instance.visitorBottomBarUIManager.VideoToggle(false);
        }
    }
}