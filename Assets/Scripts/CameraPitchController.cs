﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPitchController : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            if (transform.localRotation.x >= -0.2164f)
                transform.Rotate(Vector3.left, 20 * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            if (transform.localRotation.x <= 0.2164f)
                transform.Rotate(Vector3.right, 20 * Time.deltaTime);
        }
    }
}
