﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualAnimationController : MonoBehaviour
{
    private void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag == "Pump")
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if (hit.transform.GetChild(0).GetComponent<Animator>().speed > 0)
                    {
                        hit.transform.GetChild(0).GetComponent<Animator>().speed = 0f;
                    }
                    else
                    {
                        hit.transform.GetChild(0).GetComponent<Animator>().speed = 0.75f;
                    }
                }
            }
        }
    }
}