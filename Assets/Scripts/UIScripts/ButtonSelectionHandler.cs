﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSelectionHandler : MonoBehaviour
{
    public void ToggleSelection()
    {
        if(transform.parent.GetChild(0).gameObject.activeInHierarchy)
        {
            //Deselect
            Deselect();
        }
        else
        {
            //Select
            Select();
        }
    }

    public void Select()
    {
        GetComponent<Image>().color = new Color32(0, 0, 0, 255);
        transform.parent.GetChild(0).gameObject.SetActive(true);
    }

    public void Deselect()
    {
        GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        transform.parent.GetChild(0).gameObject.SetActive(false);
    }
}