﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HostBottomBarUIManager : MonoBehaviour
{
    [Header("Non Meeting UI")]
    public GameObject nonMeetingUIParent;
    public GameObject watchVideoButton;
    public GameObject startMeetingButton;

    [Header("Non Meeting UI Components")]
    public GameObject startMeetingComponent;
    public GameObject joinMeetingComponent;

    [Header("Meeting UI")]
    public GameObject meetingUIParent;
    public GameObject showVideoButton;
    public GameObject showPresentationButton;
    public GameObject callProductsButton;

    private void Update()
    {
        if (MeetingManager.Instance.attendeeCountValue <= 0)
        {
            if (!startMeetingComponent.activeInHierarchy)
            {
                startMeetingComponent.SetActive(true);
                joinMeetingComponent.SetActive(false);
            }
        }
        else
        {
            if (!joinMeetingComponent.activeInHierarchy)
            {
                startMeetingComponent.SetActive(false);
                joinMeetingComponent.SetActive(true);
            }
        }
    }


    public void VideoToggle(bool sendOverNetwork)
    {
        if (MeetingManager.Instance.modelsOnTable)
        {
            MeetingManager.Instance.CallProductsFromTableToVooth(false);
            UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().callProductsButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
        }

        if (AppManager.Instance.hostPresentationPanel.activeInHierarchy)
        {
            AppManager.Instance.hostPresentationPanel.SetActive(false);
            UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().showPresentationButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
        }


        if (AppManager.Instance.videoPanel.activeInHierarchy)
        {
            AppManager.Instance.ToggleVideoPanel(true);

            if (sendOverNetwork)
                PhotonNetworkManager.Instance.SendVideoToggle(true);
        }
        else
        {
            AppManager.Instance.ToggleVideoPanel(false);

            if (sendOverNetwork)
                PhotonNetworkManager.Instance.SendVideoToggle(false);
        }
    }

    public void PresentationToggle(bool sendOverNetwork)
    {
        if (MeetingManager.Instance.modelsOnTable)
        {
            MeetingManager.Instance.CallProductsFromTableToVooth(false);
            UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().callProductsButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
        }

        if (AppManager.Instance.videoPanel.activeInHierarchy)
        {
            AppManager.Instance.ToggleVideoPanel(true);
            UIHandler.Instance.hostBottomBarUIManager.GetComponent<HostBottomBarUIManager>().showVideoButton.GetComponent<ButtonSelectionHandler>().ToggleSelection();
        }

        if (AppManager.Instance.hostPresentationPanel.activeInHierarchy)
        {
            AppManager.Instance.hostPresentationPanel.SetActive(false);
        }
        else
        {
            AppManager.Instance.hostPresentationPanel.SetActive(true);
        }

        if (sendOverNetwork)
            PhotonNetworkManager.Instance.SendPresentationToggle();
    }

    public void StartMeetingToggle()
    {
        if (AppManager.Instance.sendMeetingInvitationPanel.activeInHierarchy)
        {
            AppManager.Instance.sendMeetingInvitationPanel.SetActive(false);
        }
        else
        {
            AppManager.Instance.sendMeetingInvitationPanel.SetActive(true);
        }
    }

    public void ToggleMeetingUI()
    {
        if (nonMeetingUIParent.activeInHierarchy)
        {
            nonMeetingUIParent.SetActive(false);
            meetingUIParent.SetActive(true);
        }
        else
        {
            nonMeetingUIParent.SetActive(true);
            meetingUIParent.SetActive(false);
        }
    }
}