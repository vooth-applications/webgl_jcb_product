﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHandler : MonoBehaviour
{
    public static UIHandler Instance;

    [Header("Host UI references")]
    public HostTopBarUIManager hostTopBarUIManager;
    public HostBottomBarUIManager hostBottomBarUIManager;
    public HostScreenUIManager hostScreenUIManager;

    [Header("Visitor UI references")]
    public VisitorTopBarUIManager visitorTopBarUIManager;
    public VisitorBottomBarUIManager visitorBottomBarUIManager;
    public VisitorScreenUIManager visitorScreenUIManager;

    private void Awake()
    {
        Instance = this;
    }
}