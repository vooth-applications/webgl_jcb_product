﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class VisitorTopBarUIManager : MonoBehaviour
{
    public RectTransform profileMenu;

    public bool profileMenuOpened;

    public void ProfileMenuToggle()
    {
        Debug.Log("ProfileMenuToggle is called");
        if (profileMenuOpened)
        {
            profileMenuOpened = false;
            profileMenu.DOAnchorPosY(1100, 0.5f);
        }
        else
        {
            profileMenuOpened = true;
            profileMenu.DOAnchorPosY(0, 0.5f);
        }
    }
}