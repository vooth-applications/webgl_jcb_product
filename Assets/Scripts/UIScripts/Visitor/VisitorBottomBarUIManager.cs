﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisitorBottomBarUIManager : MonoBehaviour
{
    [Header("Non Meeting UI")]
    public GameObject nonMeetingUIParent;
    public GameObject watchVideoButton;

    [Header("Non Meeting UI Components")]
    public GameObject joinMeetingComponent;

    [Header("Meeting UI")]
    public GameObject meetingUIParent;

    private void Update()
    {
        if (MeetingManager.Instance.attendeeCountValue > 0)
        {
            if (!joinMeetingComponent.activeInHierarchy)
                joinMeetingComponent.SetActive(true);
        }
        else
        {
            if (joinMeetingComponent.activeInHierarchy)
                joinMeetingComponent.SetActive(false);
        }
    }

    public void VideoToggle(bool sendOverNetwork)
    {
        if (AppManager.Instance.videoPanel.activeInHierarchy)
        {
            AppManager.Instance.ToggleVideoPanel(true);

            if (sendOverNetwork)
                PhotonNetworkManager.Instance.SendVideoToggle(true);
        }
        else
        {
            AppManager.Instance.ToggleVideoPanel(false);

            if (sendOverNetwork)
                PhotonNetworkManager.Instance.SendVideoToggle(false);
        }
    }

    public void PresentationToggle()
    {
        if (AppManager.Instance.visitorPresentationPanel.activeInHierarchy)
        {
            AppManager.Instance.visitorPresentationPanel.SetActive(false);
        }
        else
        {
            AppManager.Instance.visitorPresentationPanel.SetActive(true);
        }
    }

    public void ToggleMeetingUI()
    {
        if (nonMeetingUIParent.activeInHierarchy)
        {
            nonMeetingUIParent.SetActive(false);
            meetingUIParent.SetActive(true);
        }
        else
        {
            nonMeetingUIParent.SetActive(true);
            meetingUIParent.SetActive(false);
        }
    }
}