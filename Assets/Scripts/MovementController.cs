﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;

public class MovementController : MonoBehaviourPun
{
    [System.Serializable]
    public class JoystickButton
    {
        public Image backgroundCircle;
        public Image mainButton;
        public Rect defaultArea;
        public Vector2 touchOffset;
        public Vector2 currentTouchPos;
        public int touchID;
        public bool isActive = false;
    }

    //Move joystick data
    public JoystickButton moveTouch = new JoystickButton();
    //Use this in your movement script for the input control
    public Vector2 moveDirection;

    public GameObject meetingPosition;
    public Vector2 currentResolution;
    public float movementSpeed;
    public float minX;
    public float maxX;
    public float minZ;
    public float maxZ;

    private void Start()
    {
        moveTouch.backgroundCircle = GameObject.Find("/JoystickCanvas/MainPanel/MovementCircle").GetComponent<Image>();

        moveTouch.mainButton = GameObject.Find("/JoystickCanvas/MainPanel/MovementHandle").GetComponent<Image>();

        moveTouch.mainButton.gameObject.GetComponent<PositionLimiter>().enabled = true;

        //Save the default location of the joystick button to be used later for input detection
        moveTouch.defaultArea = new Rect(moveTouch.mainButton.rectTransform.position.x,
            moveTouch.mainButton.rectTransform.position.y,
            moveTouch.mainButton.rectTransform.sizeDelta.x,
            moveTouch.mainButton.rectTransform.sizeDelta.y);

        currentResolution = new Vector2(Screen.width, Screen.height);
    }

    void Update()
    {
        //Debug.Log("Player : " + this.gameObject.name + " , Position : " + transform.localPosition);

        if (currentResolution != new Vector2(Screen.width, Screen.height))
        {
            currentResolution = new Vector2(Screen.width, Screen.height);
            CalculateDefaultArea();
        }

        if (PhotonNetworkManager.Instance.playerInitiated)
        {
            // Check for allowing movement on local player only.
            if (photonView.IsMine)
            {
                //Handle joystick movement
#if (UNITY_ANDROID || UNITY_IOS || UNITY_WP8 || UNITY_WP8_1) && !UNITY_EDITOR
        //Mobile touch input
        for (var i = 0; i < Input.touchCount; ++i)
        {
            Touch touch = Input.GetTouch(i);

            if (touch.phase == TouchPhase.Began)
            {
                MobileButtonsCheck(new Vector2(touch.position.x, Screen.height - touch.position.y), touch.fingerId);
            }

            if (touch.phase == TouchPhase.Moved )
            {
                if(moveTouch.isActive && moveTouch.touchID == touch.fingerId)
                {
                    moveTouch.currentTouchPos = touch.position;
                }
            }

            if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {
                MobileButtonStop(touch.fingerId);
            }
        }
#else
                //Desktop mouse input for editor testing
                if (Input.GetMouseButtonDown(0))
                {
                    MobileButtonsCheck(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y), -1);
                }

                if (Input.GetMouseButtonUp(0))
                {
                    MobileButtonStop(-1);
                }

                moveTouch.currentTouchPos = Input.mousePosition;

                if (!MeetingManager.Instance.meeting)
                {
                    // Move Front / Back
                    if (moveDirection.y != 0)
                    {
                        if (transform.localPosition.x < maxX && transform.localPosition.x > minX && transform.localPosition.z < maxZ && transform.localPosition.z > minZ)
                        {
                            transform.Translate(transform.forward * Time.deltaTime * movementSpeed * moveDirection.y, Space.World);
                        }
                        else
                        {
                            if (transform.localPosition.x >= maxX)
                            {
                                transform.localPosition = new Vector3(transform.localPosition.x - 0.15f, transform.localPosition.y, transform.localPosition.z);
                                transform.Translate(transform.forward * Time.deltaTime * movementSpeed * moveDirection.y, Space.World);
                            }
                            else if (transform.localPosition.x <= minX)
                            {
                                transform.localPosition = new Vector3(transform.localPosition.x + 0.15f, transform.localPosition.y, transform.localPosition.z);
                                transform.Translate(transform.forward * Time.deltaTime * movementSpeed * moveDirection.y, Space.World);
                            }
                            else if (transform.localPosition.z >= maxZ)
                            {
                                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z - 0.15f);
                                transform.Translate(transform.forward * Time.deltaTime * movementSpeed * moveDirection.y, Space.World);
                            }
                            else if (transform.localPosition.z <= minZ)
                            {
                                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + 0.15f);
                                transform.Translate(transform.forward * Time.deltaTime * movementSpeed * moveDirection.y, Space.World);
                            }
                        }
                    }

                    if (Input.GetAxis("Vertical") != 0)
                    {
                        if (transform.localPosition.x < maxX && transform.localPosition.x > minX && transform.localPosition.z < maxZ && transform.localPosition.z > minZ)
                        {
                            transform.Translate(transform.forward * Time.deltaTime * movementSpeed * Input.GetAxis("Vertical"), Space.World);
                        }
                        else
                        {
                            if (transform.localPosition.x >= maxX)
                            {
                                transform.localPosition = new Vector3(transform.localPosition.x - 0.15f, transform.localPosition.y, transform.localPosition.z);
                                transform.Translate(transform.forward * Time.deltaTime * movementSpeed * Input.GetAxis("Vertical"), Space.World);
                            }
                            else if (transform.localPosition.x <= minX)
                            {
                                transform.localPosition = new Vector3(transform.localPosition.x + 0.15f, transform.localPosition.y, transform.localPosition.z);
                                transform.Translate(transform.forward * Time.deltaTime * movementSpeed * Input.GetAxis("Vertical"), Space.World);
                            }
                            else if (transform.localPosition.z >= maxZ)
                            {
                                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z - 0.15f);
                                transform.Translate(transform.forward * Time.deltaTime * movementSpeed * Input.GetAxis("Vertical"), Space.World);
                            }
                            else if (transform.localPosition.z <= minZ)
                            {
                                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + 0.15f);
                                transform.Translate(transform.forward * Time.deltaTime * movementSpeed * Input.GetAxis("Vertical"), Space.World);
                            }
                        }
                    }
                }

                //Rotate Left/Right
                if (moveDirection.x != 0)
                {
                    transform.Rotate(new Vector3(0, 14, 0) * Time.deltaTime * 4.5f * moveDirection.x, Space.Self);
                }

                if (Input.GetAxis("Horizontal") != 0)
                {
                    transform.Rotate(new Vector3(0, 14, 0) * Time.deltaTime * 4.5f * Input.GetAxis("Horizontal"), Space.Self);
                }

#endif

                //Moving
                if (moveTouch.isActive)
                {
                    moveTouch.mainButton.rectTransform.position = new Vector3(moveTouch.currentTouchPos.x - moveTouch.touchOffset.x, moveTouch.currentTouchPos.y - moveTouch.touchOffset.y);
                    moveDirection.x = moveTouch.mainButton.rectTransform.position.x - moveTouch.defaultArea.x;
                    moveDirection.y = moveTouch.mainButton.rectTransform.position.y - moveTouch.defaultArea.y;

                    if (Mathf.Abs(moveDirection.x) < 19)
                    {
                        moveDirection.x = 0;
                    }
                    else
                    {
                        moveDirection.x = Mathf.Clamp(moveDirection.x / 75.000f, -1.000f, 1.000f);
                    }

                    if (Mathf.Abs(moveDirection.y) < 19)
                    {
                        moveDirection.y = 0;
                    }
                    else
                    {
                        moveDirection.y = Mathf.Clamp(moveDirection.y / 75.000f, -1.000f, 1.000f);
                    }
                }
                else
                {
                    moveTouch.mainButton.rectTransform.position = new Vector3(moveTouch.defaultArea.x, moveTouch.defaultArea.y);
                    moveDirection = Vector2.zero;
                }
            }
        }
    }

    //Here we check if the clicked/tapped position is inside the joystick button
    void MobileButtonsCheck(Vector2 touchPos, int touchID)
    {
        //Move controller
        if (moveTouch.defaultArea.Contains(new Vector2(touchPos.x, Screen.height - touchPos.y)) && !moveTouch.isActive)
        {
            moveTouch.isActive = true;
            moveTouch.touchOffset = new Vector2(touchPos.x - moveTouch.defaultArea.x, Screen.height - touchPos.y - moveTouch.defaultArea.y);
            moveTouch.currentTouchPos = new Vector2(touchPos.x, Screen.height - touchPos.y);
            moveTouch.touchID = touchID;
        }
    }

    //Here we release the previously active joystick if we release the mouse button/finger from the screen
    void MobileButtonStop(int touchID)
    {
        if (moveTouch.isActive && moveTouch.touchID == touchID)
        {
            moveTouch.isActive = false;
            moveTouch.touchOffset = Vector2.zero;
            moveTouch.touchID = -1;
        }
    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        object[] customData = info.photonView.InstantiationData;

        info.photonView.gameObject.transform.Find("Canvas/Name").GetComponent<TextMeshProUGUI>().text = customData[0].ToString();

        if (UIManagerOld.Instance.errorShown)
        {
            StartCoroutine(UIManagerOld.Instance.DisplayerErrorTextAfterDelay(customData[0].ToString() + " has joined"));
        }
        else
        {
            UIManagerOld.Instance.DisplayErrorText(customData[0].ToString() + " has joined");
        }
    }

    public void CalculateDefaultArea()
    {
        Debug.Log("CalculateDefaultArea is called");

        //Save the default location of the joystick button to be used later for input detection
        moveTouch.defaultArea = new Rect(moveTouch.mainButton.rectTransform.position.x,
            moveTouch.mainButton.rectTransform.position.y,
            moveTouch.mainButton.rectTransform.sizeDelta.x,
            moveTouch.mainButton.rectTransform.sizeDelta.y);
    }
}