﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionLimiter : MonoBehaviour
{
    float radius = 80; //radius of *black circle*
    public Vector3 centerPosition;
    private void OnEnable()
    {
        centerPosition = transform.localPosition; //center of *black circle*
    }

    private void Start()
    {
        
    }

    //private void Start()
    //{
    //    centerPosition = transform.localPosition; //center of *black circle*
    //}

    public void Update()
    {
        float distance = Vector3.Distance(transform.localPosition, centerPosition); //distance from ~green object~ to *black circle*

        if (distance > radius) //If the distance is less than the radius, it is already within the circle.
        {
            Vector3 fromOriginToObject = transform.localPosition - centerPosition; //~GreenPosition~ - *BlackCenter*
            fromOriginToObject *= radius / distance; //Multiply by radius //Divide by Distance

            transform.localPosition = centerPosition + fromOriginToObject; //*BlackCenter* + all that Math  
        }
    }
}