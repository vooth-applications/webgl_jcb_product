﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChairProperties : MonoBehaviour
{
    public bool chairTaken;
    public Vector3 chairSittingPosition;

    public Vector3 OccupyChair()
    {
        chairTaken = true;

        return chairSittingPosition;
    }

    public void LeaveChair()
    {
        chairTaken = false;
    }
}