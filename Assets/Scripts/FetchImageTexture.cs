﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;

public class FetchImageTexture : MonoBehaviour
{
    public Image slideImage;

    private Sprite targetSprite;
    public string url;
    public TextMeshProUGUI loadingText;

    private void Awake()
    {
        slideImage = GetComponent<Image>();
        loadingText = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        loadingText.text = "Loading...";
    }

    private void Start()
    {
        StartCoroutine(GetTextureRequest(url, (response) =>
        {
            targetSprite = response;
            slideImage.sprite = targetSprite;
            loadingText.text = "";
        }));
    }

    IEnumerator GetTextureRequest(string url, System.Action<Sprite> callback)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    var texture = DownloadHandlerTexture.GetContent(www);
                    var rect = new Rect(0, 0, 1920f, 1080f);
                    var sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f));
                    callback(sprite);
                }
            }
        }
    }
}
