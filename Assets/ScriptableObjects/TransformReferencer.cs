﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TransformReferencer", menuName = "ScriptableObjects/TransformReferencer", order = 1)]
public class TransformReferencer : ScriptableObject
{
    [Header("Host Environment Initial Transforms")]
    public GameObject host1InitialTransform;
    public GameObject host2InitialTransform;
    public GameObject host3InitialTransform;

    [Header("Visitor Environment Initial Transforms")]
    public GameObject visitor1InitialTransform;
    public GameObject visitor2InitialTransform;
    public GameObject visitor3InitialTransform;

    [Header("Host Environment Meeting Transforms")]
    public GameObject host1MeetingAttendedPosition;
    public GameObject host2MeetingAttendedPosition;
    public GameObject host3MeetingAttendedPosition;

    [Header("Host Environment Meeting Transforms")]
    public GameObject visitor1MeetingAttendedPosition;
    public GameObject visitor2MeetingAttendedPosition;
    public GameObject visitor3MeetingAttendedPosition;

    [HideInInspector]
    public string HOST1IT = "HOST1IT";
    [HideInInspector]
    public string HOST2IT = "HOST2IT";
    [HideInInspector]
    public string HOST3IT = "HOST3IT";
    [HideInInspector]
    public string VISITOR1IT = "VISITOR1IT";
    [HideInInspector]
    public string VISITOR2IT = "VISITOR2IT";
    [HideInInspector]
    public string VISITOR3IT = "VISITOR3IT";

    [HideInInspector]
    public string HOST1MT = "HOST1MT";
    [HideInInspector]
    public string HOST2MT = "HOST2MT";
    [HideInInspector]
    public string HOST3MT = "HOST3MT";
    [HideInInspector]
    public string VISITOR1MT = "VISITOR1MT";
    [HideInInspector]
    public string VISITOR2MT = "VISITOR2MT";
    [HideInInspector]
    public string VISITOR3MT = "VISITOR3MT";
    [HideInInspector]
    public string ATTENDEECOUNT = "ATTENDEECOUNT";
}